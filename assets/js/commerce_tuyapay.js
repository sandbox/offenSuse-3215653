(function ($, Drupal) {
  Drupal.behaviors.TuyaPayModal = {
    attach: function (context, settings) {
      if ($('.tuyapay-modal-link', context).length && !$('.tuyapay-modal-link').hasClass('pressed')) {
        $('.tuyapay-modal-link').toggleClass('pressed').click();
      }
    }
  };

}(jQuery, Drupal));
