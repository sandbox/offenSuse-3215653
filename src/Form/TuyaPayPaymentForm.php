<?php

namespace Drupal\commerce_tuyapay\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_checkout\CheckoutOrderManagerInterface;
use Drupal\commerce_tuyapay\TuyaPayUtilitiesInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Url;

/**
 * Define the TuyaPay form.
 *
 * @package Drupal\commerce_tuyapay\Form
 */
class TuyaPayPaymentForm extends FormBase {

  /**
   * Defines the interface for orders.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * Defines the interface for payment gateway configuration entities.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface
   */
  protected $gateway;

  /**
   * Provides functionality for handling an order's checkout.
   *
   * @var \Drupal\commerce_checkout\CheckoutOrderManagerInterface
   */
  protected $checkoutOrderManager;

  /**
   * Provides an interface for entity type managers.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Defines an interface for a service which has the current account stored.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * Define interface for TuyaPay commerce.
   *
   * @var \Drupal\commerce_tuyapay\TuyaPayUtilitiesInterface
   */
  protected $tuyaPayUtilities;

  /**
   * Stores runtime messages sent out to individual users on the page.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new OrderReassignForm object.
   *
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The current route match.
   * @param \Drupal\commerce_checkout\CheckoutOrderManagerInterface $checkout_order_manager
   *   The checkout order manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current user account.
   * @param \Drupal\commerce_tuyapay\TuyaPayUtilitiesInterface $tuya_pay_utilities
   *   Define interface for TuyaPay commerce.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Stores runtime messages sent out to individual users on the page.
   */
  public function __construct(
    CurrentRouteMatch $current_route_match,
    CheckoutOrderManagerInterface $checkout_order_manager,
    EntityTypeManagerInterface $entity_type_manager,
    AccountProxyInterface $account,
    TuyaPayUtilitiesInterface $tuya_pay_utilities,
    MessengerInterface $messenger
  ) {
    $this->order = $current_route_match->getParameter('commerce_order');
    $this->gateway = $current_route_match->getParameter('commerce_payment_gateway');
    $this->checkoutOrderManager = $checkout_order_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->account = $account;
    $this->messenger = $messenger;
    $this->tuyaPayUtilities = $tuya_pay_utilities;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('commerce_checkout.checkout_order_manager'),
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('commerce_tuyapay.utilities'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tuyapay_payment_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#prefix'] = '<div id="tuyapay-payment-ajax">';
    $form['#suffix'] = '</div>';
    $gatewayConfiguration = $this->gateway->getPluginConfiguration();

    $step = $form_state->get('step');
    if (!$step) {
      $step = 'tuyapay';
      $form_state->set('step', $step);
    }

    $form['image'] = [
      '#theme' => 'image',
      '#uri' => drupal_get_path('module', 'commerce_tuyapay') . '/assets/images/tuyalogo.svg',
      '#prefix' => '<div class="modal-image-title">',
      '#suffix' => '</div>',
    ];

    switch ($step) {
      case 'tuyapay':
        $form['user_data'] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => [
              'user-data-group',
            ],
          ],
        ];

        $form['user_data']['phone_number'] = [
          '#type' => 'tel',
          '#title' => $this->t('Phone number'),
          '#require' => TRUE,
        ];

        $form['user_data']['document_type'] = [
          '#type' => 'select',
          '#title' => $this->t('Identification type'),
          '#empty_option' => $this->t('- Identification type -'),
          '#options' => [
            1 => $this->t('Citizenship card'),
            2 => $this->t('Inmigration card'),
            3 => $this->t('NIT'),
            4 => $this->t('Identity card'),
          ],
          '#require' => TRUE,
        ];

        $form['user_data']['document_number'] = [
          '#type' => 'tel',
          '#title' => $this->t('Identification number'),
          '#require' => TRUE,
        ];

        $form['instructions'] = [
          '#markup' => '<div class="tuyapayment-pin-instructions"><h2>' . $this->t('Online payment PIN') . '</h2>
          <p>' . $this->t('Copy and paste here the online payment PIN that you generate in the TuyaPay app') . '</p></div>',
        ];

        $form['pin_code'] = [
          '#type' => 'tel',
          '#title' => $this->t('Online payment PIN'),
          '#require' => TRUE,
          '#default_value' => NULL,
          '#maxlength' => 6,
          '#prefix' => '<div id="code-output">',
          '#suffix' => '</div>',
        ];

        $form['actions'] = [
          '#type' => 'actions',
        ];

        if ($gatewayConfiguration['mode'] == 'test') {
          $form['actions']['otp_code_generate'] = [
            '#type' => 'submit',
            '#value' => $this->t('Generate OTP'),
            '#submit' => ['::generateOtp'],
            '#limit_validation_errors' => [],
            '#ajax' => [
              'callback' => '::generateOtpAjax',
              'wrapper' => 'code-output',
            ],
            '#attributes' => [
              'class' => [
                'otp-code-generate',
              ],
            ],
          ];
        }

        $form['actions']['cancel'] = [
          '#type' => 'submit',
          '#value' => $this->t('Cancel'),
          '#limit_validation_errors' => [],
          '#submit' => ['::cancelPayment'],
          '#weight' => 80,
          '#ajax' => [
            'callback' => '::cancelPaymentAjax',
            'wrapper' => 'tuyapay-payment-ajax',
          ],
          '#attributes' => [
            'class' => [
              'btn-cancel',
            ],
          ],
          '#name' => 'cancel-initial',
        ];

        $form['actions']['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Siguiente'),
          '#weight' => 81,
          '#ajax' => [
            'callback' => '::cashOutRechargerAjax',
            'wrapper' => 'tuyapay-payment-ajax',
          ],
          '#submit' => ['::cashOutRecharger'],
          '#name' => 'submit-initial',
        ];
        break;

      case 'try_again':
        $message = $form_state->get('error_msg');
        $form_state->set('error_msg', NULL);
        if (empty($message)) {
          $message = $this->t('Internal service error');
        }

        $form['message'] = [
          '#markup' => '<div class="error-message">' . $message . '</div>',
        ];

        $form['actions'] = [
          '#type' => 'actions',
        ];

        if ($step == 'error') {
          $form['actions']['cancel'] = [
            '#type' => 'submit',
            '#value' => $this->t('Finish'),
            '#limit_validation_errors' => [],
            '#submit' => ['::cancelPayment'],
            '#weight' => 80,
            '#ajax' => [
              'callback' => '::cancelPaymentAjax',
              'wrapper' => 'tuyapay-payment-ajax',
            ],
            '#attributes' => [
              'class' => [
                'btn-cancel',
              ],
            ],
            '#name' => 'cancel-initial',
          ];
        }
        else {
          $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Try again'),
            '#weight' => 98,
            '#ajax' => [
              'callback' => '::tryAgainAjax',
              'wrapper' => 'tuyapay-payment-ajax',
            ],
            '#submit' => ['::tryAgain'],
            '#name' => 'submit-incorrect-otp',
          ];
        }

        break;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function generateOtp(array &$form, FormStateInterface $form_state) {
    $gatewayConfiguration = $this->gateway->getPluginConfiguration();
    $userInput = $form_state->getUserInput();
    $oauthToken = $this->tuyaPayUtilities->generateToken($gatewayConfiguration['oauth_endpoint'], $gatewayConfiguration['tuyapay_subs_key'], [
      'grant_type' => $gatewayConfiguration['grant_type'],
      'client_id' => $gatewayConfiguration['client_id'],
      'scope' => $gatewayConfiguration['scope'],
      'client_secret' => $gatewayConfiguration['client_secret'],
    ]);

    if ($oauthToken) {
      $idType = $this->order->field_tdoc->value;
      $otpData = [
        'codigoOperacion' => $gatewayConfiguration['operation_code'],
        'tokenIdempotencia' => sha1(mt_rand(1, 90000) . 'SALT'),
        'DeviceID' => $this->order->getCustomerId(),
        'numeroCelular' => $userInput['phone_number'],
        'numeroIdentificacion' => $userInput['document_number'],
        'tipoIdentificacion' => $idType,
        'codigoAliado' => (int) $gatewayConfiguration['partner_code'],
        'codigoCanal' => $gatewayConfiguration['channel_code'],
      ];
      $otpCode = $this->tuyaPayUtilities->generateOtpCode($gatewayConfiguration['otp_endpoint'], $gatewayConfiguration['tuyapay_subs_key'], $oauthToken['access_token'], $otpData);

      if (empty($otpCode['error'])) {
        $form_state->set('opt_code', $otpCode);
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function generateOtpAjax(array &$form, FormStateInterface $form_state) {
    $optCode = $form_state->get('opt_code');
    $form['pin_code']['#value'] = $optCode["resultado"]["codigoAutenticacion"];
    return $form['pin_code'];
  }

  /**
   * {@inheritdoc}
   */
  public function cashOutRecharger(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->get('error_msg'))) {
      $orderTotal = $this->order->getTotalPrice();
      $formValues = $form_state->getValues();
      $dateTime = new DrupalDateTime('now');
      $dateFormatted = $dateTime->format('Y-m-d\TH:i:s');
      $trazaId = $formValues['phone_number'] . time() . rand(10, 99);
      $gatewayConfiguration = $this->gateway->getPluginConfiguration();
      $oauthToken = $this->tuyaPayUtilities->generateToken($gatewayConfiguration['oauth_endpoint'], $gatewayConfiguration['tuyapay_subs_key'], [
        'grant_type' => $gatewayConfiguration['grant_type'],
        'client_id' => $gatewayConfiguration['client_id'],
        'scope' => $gatewayConfiguration['scope'],
        'client_secret' => $gatewayConfiguration['client_secret'],
      ]);

      $data = [
        'trazaId' => $trazaId,
        'CodigoAliado' => (int) $gatewayConfiguration['partner_code'],
        'CodigoCanal' => $gatewayConfiguration['channel_code'],
        'tipotransaccion' => (int) $gatewayConfiguration['transaction_type_number'],
        'codigoUnico' => $gatewayConfiguration['unique_code'],
        'codigoAlmacen' => $gatewayConfiguration['store_code'],
        'referencia1' => '',
        'referencia2' => '',
        'transaccion' => [
          'ubicacionId' => $gatewayConfiguration['location_id'],
          'fechaTransaccion' => $dateFormatted,
          'valor' => (int) $orderTotal->getNumber(),
          'tipo' => $gatewayConfiguration['transaction_type'],
        ],
        'cliente' => [
          'codigoAutorizacion' => $formValues['pin_code'],
          'clienteId' => $formValues['document_number'],
          'tipoDocumento' => (int) $formValues['document_type'],
          'celular' => $formValues['phone_number'],
          'llaveTransaccion' => $formValues['phone_number'],
        ],
        'impuestos' => [
          'base_IVA' => 0,
          'iva' => $this->calculateTax(),
          'impoconsumo' => 0,
          'propina' => 0,
          't_aeroportuaria' => 0,
        ],
      ];

      $response = $this->tuyaPayUtilities->cashOutRecharger(
        $gatewayConfiguration['recharger_endpoint'],
        $gatewayConfiguration['tuyapay_subs_key'],
        $oauthToken['access_token'],
        $gatewayConfiguration['connection_timeout'],
        $data
      );

      if (empty($response['error'])) {
        $paymentStorage = $this->entityTypeManager->getStorage('commerce_payment');
        $payment = $paymentStorage->create([
          'state' => 'completed',
          'amount' => $orderTotal,
          'payment_gateway' => $this->gateway->id(),
          'order_id' => $this->order->id(),
          'remote_id' => $response['resultado']['codigoVerificacion'],
          'remote_state' => 'completed',
        ]);
        $payment->save();
        $form_state->set('finishRedirect', $this->buildReturnUrl()->toString());
        return;
      }

      $errorCode = $response['response']['resultado']['codigoRespuesta'];
      if ($errorCode === 5704) {
        $form_state->set('step', 'try_again');
        $form_state->set('error_msg', $this->t('The PIN code is incorrect'));
        $form_state->setRebuild();
        return;
      }

      $this->messenger->addMessage($response['response']['resultado']['descripcionRespuesta']);
      $form_state->set('finishRedirect', $this->buildCancelUrl()->toString());
      return;
    }

    $this->messenger->addMessage($this->t('TuyaPay error service.'));
    $form_state->set('finishRedirect', $this->buildCancelUrl()->toString());
  }

  /**
   * {@inheritdoc}
   */
  public function cashOutRechargerAjax(array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->get('finishRedirect'))) {
      $response = new AjaxResponse();
      $response->addCommand(new RedirectCommand($form_state->get('finishRedirect')));
      return $response;
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function tryAgainAjax(array &$form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function tryAgain(array &$form, FormStateInterface $form_state) {
    $form_state->set('step', 'tuyapay');
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function cancelPaymentAjax(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new CloseModalDialogCommand());
    $response->addCommand(new RedirectCommand($this->buildCancelUrl()->toString()));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function cancelPayment(array &$form, FormStateInterface $form_state) {
    $this->messenger->addMessage($this->t('You have canceled checkout at TuyaPay but may resume the checkout process here when you are ready.'));
  }

  /**
   * Calculate the TAX.
   *
   * @return float
   *   The calculated tax.
   */
  protected function calculateTax() {
    $tax_amount = 0;
    foreach ($this->order->getItems() as $order_item) {
      foreach ($order_item->getAdjustments() as $adjustment) {
        if ($adjustment->getType() == 'tax') {
          $tax_amount += $adjustment->getAmount()->getNumber();
        }
      }
    }
    return $tax_amount;
  }

  /**
   * Builds the URL to the "return" page.
   *
   * @return \Drupal\Core\Url
   *   The "return" page URL.
   */
  protected function buildReturnUrl() {
    return Url::fromRoute('commerce_payment.checkout.return', [
      'commerce_order' => $this->order->id(),
      'step' => 'payment',
    ], ['absolute' => TRUE]);
  }

  /**
   * Builds the URL to the "cancel" page.
   *
   * @return \Drupal\Core\Url
   *   The "cancel" page URL.
   */
  protected function buildCancelUrl() {
    return Url::fromRoute('commerce_payment.checkout.cancel', [
      'commerce_order' => $this->order->id(),
      'step' => 'payment',
    ], ['absolute' => TRUE]);
  }

}
