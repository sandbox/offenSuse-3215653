<?php

namespace Drupal\commerce_tuyapay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_checkout\CheckoutOrderManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Discovery\YamlDiscovery;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "tuyapay",
 *   label = "Tuya Pay",
 *   display_label = "Tuya Pay",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_tuyapay\PluginForm\OffsiteRedirect\TuyaPayForm",
 *   },
 *   payment_method_types = {"payment_tuyapay"},
 *   requires_billing_information = FALSE,
 * )
 */
class TuyaPay extends OffsitePaymentGatewayBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal renderer service.
   *
   * @var Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The checkout order manager.
   *
   * @var \Drupal\commerce_checkout\CheckoutOrderManagerInterface
   */
  protected $checkoutOrderManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;


  /**
   * The collection of Payu oficial Images.
   *
   * @var array
   */
  protected $images;

  /**
   * The configuration of the module.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Constructs a new PayuWebcheckout object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Drupal renderer service.
   * @param \Drupal\commerce_checkout\CheckoutOrderManagerInterface $checkout_order_manager
   *   The checkout order manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    ModuleHandlerInterface $module_handler,
    RendererInterface $renderer,
    CheckoutOrderManagerInterface $checkout_order_manager,
    RouteMatchInterface $route_match,
    ConfigFactoryInterface $config_factory
  ) {
    $this->moduleHandler = $module_handler;
    $this->renderer = $renderer;
    $this->checkoutOrderManager = $checkout_order_manager;
    $this->routeMatch = $route_match;
    $this->config = $config_factory->get('commerce_tuyapay.tuyapay_settings');
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $payment_type_manager,
      $payment_method_type_manager,
      $time
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('module_handler'),
      $container->get('renderer'),
      $container->get('commerce_checkout.checkout_order_manager'),
      $container->get('current_route_match'),
      $container->get('config.factory')
    );
  }

  /**
   * Obtains Images set in yaml.
   *
   * @return array
   *   An array with images found in Yaml files
   *   whose keys are the image key and whose
   *   value, is the actual image path to use.
   */
  protected function tuyaPayImages() {
    if ($this->images) {
      return $this->images;
    }
    $discovery = new YamlDiscovery('tuyapay_images', $this->moduleHandler->getModuleDirectories());
    $definitions = $discovery->findAll();
    $this->images = [];
    foreach ($definitions as $definition) {
      foreach ($definition as $key => $image) {
        $this->images[$key] = $image;
      }
    }
    return $this->tuyaPayImages();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'image' => '',
      'tuyapay_subs_key' => '',
      'partner_code' => '',
      'channel_code' => '',
      'transaction_type_number' => '',
      'unique_code' => '',
      'store_code' => '',
      'location_id' => '',
      'transaction_type' => '',
      'oauth_endpoint' => '',
      'grant_type' => 'client_credentials',
      'client_id' => '',
      'scope' => '',
      'client_secret' => '',
      'otp_endpoint' => '',
      'operation_code' => '',
      'recharger_endpoint' => '',
      'connection_timeout' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $image_options = [];
    foreach (array_keys($this->tuyaPayImages()) as $image_key) {
      $image_options[$image_key] = ucfirst(str_replace('_', ' ', $image_key));
    }

    $form['image_key'] = [
      '#type' => 'select',
      '#title' => $this->t('Payment selection image.'),
      '#description' => $this->t('Select the image you would like to have displayed at the payment method pane.'),
      '#options' => $image_options,
      '#default_value' => $this->configuration['image_key'],
      '#empty_option' => $this->t('No image'),
      '#empty_value' => 'no_image',
    ];

    $form['general_config'] = [
      '#type' => 'details',
      '#title' => $this->t('General config'),
      '#description' => $this->t('General configuration for TuyaPay services.'),
    ];

    $form['general_config']['tuyapay_subs_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ocp-Apim-Subscription-Key'),
      '#default_value' => $this->configuration['tuyapay_subs_key'],
      '#description' => $this->t('Subscription key provided by TuyaPay'),
      '#required' => TRUE,
    ];

    $form['general_config']['partner_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Partner code'),
      '#default_value' => $this->configuration['partner_code'],
      '#description' => $this->t('Partner code provided by TuyaPay.'),
      '#required' => TRUE,
    ];

    $form['general_config']['channel_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Channel code'),
      '#default_value' => $this->configuration['channel_code'],
      '#description' => $this->t('Channel code provided by TuyaPay.'),
      '#required' => TRUE,
    ];

    $form['general_config']['transaction_type_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Transaction type number'),
      '#default_value' => $this->configuration['transaction_type_number'],
      '#description' => $this->t('transaction type number provided by TuyaPay.'),
      '#required' => TRUE,
    ];

    $form['general_config']['unique_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Unique code'),
      '#default_value' => $this->configuration['unique_code'],
      '#description' => $this->t('Unique code provided by TuyaPay.'),
      '#required' => TRUE,
    ];

    $form['general_config']['store_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Store code'),
      '#default_value' => $this->configuration['store_code'],
      '#description' => $this->t('Store code provided by TuyaPay.'),
      '#required' => TRUE,
    ];

    $form['general_config']['location_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location id'),
      '#default_value' => $this->configuration['location_id'],
      '#description' => $this->t('Location id provided by TuyaPay.'),
      '#required' => TRUE,
    ];

    $form['general_config']['transaction_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Transaction type'),
      '#default_value' => $this->configuration['transaction_type'],
      '#description' => $this->t('Transaction type provided by TuyaPay.'),
      '#required' => TRUE,
    ];

    $form['oauth_config'] = [
      '#type' => 'details',
      '#title' => $this->t('Oauth token'),
      '#description' => $this->t('Config for oAuth token service'),
    ];

    $form['oauth_config']['oauth_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('oAuth Endpoint'),
      '#default_value' => $this->configuration['oauth_endpoint'],
      '#description' => $this->t('Endpoint URL, for example: https://cerapi1.tuya.com.co/private/oauth/v2.0/token'),
      '#required' => TRUE,
    ];

    $form['oauth_config']['grant_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('oAuth Grant Type'),
      '#default_value' => $this->configuration['grant_type'],
      '#required' => TRUE,
    ];

    $form['oauth_config']['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $this->configuration['client_id'],
      '#description' => $this->t('client_id provided by TuyaPay'),
      '#required' => TRUE,
    ];

    $form['oauth_config']['scope'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scope'),
      '#default_value' => $this->configuration['scope'],
      '#description' => $this->t('scope provided by TuyaPay'),
      '#required' => TRUE,
    ];

    $form['oauth_config']['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#default_value' => $this->configuration['client_secret'],
      '#description' => $this->t('client_secret provided by TuyaPay'),
      '#required' => TRUE,
    ];

    $form['otp_config'] = [
      '#type' => 'details',
      '#title' => $this->t('OTP code'),
      '#description' => $this->t('Generate OTP code for debug.'),
    ];

    $form['otp_config']['otp_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OTP Endpoint'),
      '#default_value' => $this->configuration['otp_endpoint'],
      '#description' => $this->t('OTP for generate a code when test mode is active.'),
      '#required' => TRUE,
    ];

    $form['otp_config']['operation_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Operation code'),
      '#default_value' => $this->configuration['operation_code'],
      '#description' => $this->t('Operation code provided by TuyaPay.'),
      '#required' => TRUE,
    ];

    $form['recharger_config'] = [
      '#type' => 'details',
      '#title' => $this->t('Recharger config'),
      '#description' => $this->t('Config for recharger service'),
    ];

    $form['recharger_config']['recharger_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Recharger Endpoint'),
      '#default_value' => $this->configuration['recharger_endpoint'],
      '#description' => $this->t('Recharger URL'),
      '#required' => TRUE,
    ];

    $form['recharger_config']['connection_timeout'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Connection timeout'),
      '#default_value' => $this->configuration['connection_timeout'],
      '#description' => $this->t('Max connection time to complete the request'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $images = $this->tuyaPayImages();
      $this->configuration['image_key'] = $values['image_key'];
      $this->configuration['image'] = isset($images[$values['image_key']]) ? $images[$values['image_key']] : NULL;
      $this->configuration['tuyapay_subs_key'] = $values['general_config']['tuyapay_subs_key'];
      $this->configuration['partner_code'] = $values['general_config']['partner_code'];
      $this->configuration['channel_code'] = $values['general_config']['channel_code'];
      $this->configuration['transaction_type_number'] = $values['general_config']['transaction_type_number'];
      $this->configuration['unique_code'] = $values['general_config']['unique_code'];
      $this->configuration['store_code'] = $values['general_config']['store_code'];
      $this->configuration['location_id'] = $values['general_config']['location_id'];
      $this->configuration['transaction_type'] = $values['general_config']['transaction_type'];
      $this->configuration['oauth_endpoint'] = $values['oauth_config']['oauth_endpoint'];
      $this->configuration['grant_type'] = $values['oauth_config']['grant_type'];
      $this->configuration['client_id'] = $values['oauth_config']['client_id'];
      $this->configuration['scope'] = $values['oauth_config']['scope'];
      $this->configuration['client_secret'] = $values['oauth_config']['client_secret'];
      $this->configuration['otp_endpoint'] = $values['otp_config']['otp_endpoint'];
      $this->configuration['operation_code'] = $values['otp_config']['operation_code'];
      $this->configuration['recharger_endpoint'] = $values['recharger_config']['recharger_endpoint'];
      $this->configuration['connection_timeout'] = $values['recharger_config']['connection_timeout'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
  }

}
