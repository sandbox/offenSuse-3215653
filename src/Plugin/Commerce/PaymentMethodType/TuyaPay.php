<?php

namespace Drupal\commerce_tuyapay\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;

/**
 * Provides the TuyaPay payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "payment_tuyapay",
 *   label = @Translation("Tuya Pay"),
 * )
 */
class TuyaPay extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    return $this->t('Tuya Pay');
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();
    return $fields;
  }

}
