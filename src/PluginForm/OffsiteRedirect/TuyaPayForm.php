<?php

namespace Drupal\commerce_tuyapay\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;

/**
 * TuyaPay form.
 */
class TuyaPayForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $order = $payment->getOrder();
    $form = parent::buildConfigurationForm($form, $form_state);

    $link_url = Url::fromRoute(
      'commerce_tuyapay.payment',
      [
        'commerce_order' => $order->id(),
        'commerce_payment_gateway' => $payment->getPaymentGateway()->id(),
      ]
    );

    $link_url->setOptions([
      'attributes' => [
        'class' => [
          'use-ajax',
          'tuyapay-modal-link',
          'invisible',
          'element-invisible',
          'hidden',
        ],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode([
          'dialogClass' => 'tuyapay-modal-form',
          'width' => 480,
          'resizable' => 'w',
          'modal' => TRUE,
          'autoResize' => FALSE,
          'draggable' => FALSE,
          'drupalAutoButtons' => FALSE,
          'buttons' => [],
          'closeOnEscape' => FALSE,
        ]),
      ],
    ]);

    $form['link'] = [
      '#type' => 'link',
      '#title' => 'Pago',
      '#url' => $link_url,
    ];

    $form['#attached']['library'][] = 'commerce_tuyapay/payment';

    return $form;
  }

}
