<?php

namespace Drupal\commerce_tuyapay;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\Exception\ConnectException;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Http\ClientFactory;

/**
 * Class TuyaPay utilities.
 */
class TuyaPayUtilities implements TuyaPayUtilitiesInterface {

  use StringTranslationTrait;

  /**
   * Helper class to construct a HTTP client with Drupal specific config.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * Logger channel factory interface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Constructs a new CurrentScityEntity object.
   *
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   *   Helper class to construct a HTTP client with Drupal specific config.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger channel factory interface.
   */
  public function __construct(
    ClientFactory $http_client_factory,
    LoggerChannelFactoryInterface $logger
  ) {
    $this->httpClientFactory = $http_client_factory;
    $this->logger = $logger->get('tuyapay');
  }

  /**
   * Generate Authorization token.
   *
   * @param string $endpoint
   *   Endpoint from gateway configuration for oAuth service.
   * @param string $subscriptionKey
   *   Id provided for TuyaPay set in the gateway configuration.
   * @param array $params
   *   Collection with required data.
   *
   * @return array
   *   Service Response.
   */
  public function generateToken($endpoint, $subscriptionKey, array $params) {
    if (empty($endpoint)) {
      return ['error' => TRUE];
    }

    try {
      $url = rtrim($endpoint, '/');
      $clientOptions = [
        'verify' => FALSE,
        'http_errors' => FALSE,
        'connect_timeout' => 20,
      ];
      $client = $this->httpClientFactory->fromOptions($clientOptions);

      $headers = [
        'Ocp-Apim-Subscription-Key' => $subscriptionKey,
        'Content-Type' => 'application/x-www-form-urlencoded',
      ];

      $response = $client->post($url, [
        'headers' => $headers,
        'form_params' => $params,
      ]);

      if ($response->getStatusCode() == 200) {
        return Json::decode($response->getBody());
      }
      else {
        $this->logger->error(
          $this->t(
            "TuyaPay could not generate the oAouth.<br />%reason",
            [
              '%reason' => $response->getReasonPhrase(),
            ],
          )
        );
        return ['error' => TRUE];
      }

    }
    catch (ConnectException $e) {
      watchdog_exception('tuyapay', $e);
      return [
        'error' => TRUE,
        'response' => Json::decode($response->getBody()->getContents()),
      ];
    }

    return ['error' => TRUE];
  }

  /**
   * Generate OTP Code.
   *
   * @param string $endpoint
   *   Endpoint from gateway configuration for otp service.
   * @param string $subscriptionKey
   *   Id provided for TuyaPay set in the gateway configuration.
   * @param string $token
   *   Autorization token generated for generateToken method.
   * @param array $data
   *   Collection with required data.
   *
   * @return array
   *   Service Response.
   */
  public function generateOtpCode($endpoint, $subscriptionKey, $token, array $data) {
    if (empty($endpoint)) {
      return ['error' => TRUE];
    }

    try {
      $url = rtrim($endpoint, '/');
      $clientOptions = [
        'verify' => TRUE,
        'http_errors' => FALSE,
        'connect_timeout' => 20,
      ];
      $client = $this->httpClientFactory->fromOptions($clientOptions);

      $headers = [
        'Ocp-Apim-Subscription-Key' => $subscriptionKey,
        'Content-Type' => 'application/json',
        'Authorization' => 'Bearer ' . $token,
      ];

      $response = $client->post($url, [
        'headers' => $headers,
        'body' => Json::encode($data),
      ]);

      if ($response->getStatusCode() == 200) {
        return Json::decode($response->getBody());
      }
      else {
        $this->logger->error(
          $this->t(
            "TuyaPay could not generate the OTP Code.<br />%reason",
            [
              '%reason' => $response->getReasonPhrase(),
            ],
          )
        );
        return [
          'error' => TRUE,
          'response' => Json::decode($response->getBody()->getContents()),
        ];
      }

    }
    catch (ConnectException $e) {
      watchdog_exception('tuyapay', $e);
      return ['error' => TRUE];
    }

    return ['error' => TRUE];
  }

  /**
   * Execute recharger.
   *
   * @param string $endpoint
   *   Endpoint from gateway configuration for recharger service.
   * @param string $subscriptionKey
   *   Id provided for TuyaPay set in the gateway configuration.
   * @param string $token
   *   Autorization token generated for generateToken method.
   * @param int $connectionTimeout
   *   Max connection time to complete the request.
   * @param array $data
   *   Collection with required data.
   *
   * @return array
   *   Service Response.
   */
  public function cashOutRecharger($endpoint, $subscriptionKey, $token, $connectionTimeout, array $data) {
    if (empty($endpoint)) {
      return ['error' => TRUE];
    }

    try {
      $url = rtrim($endpoint, '/');
      $clientOptions = [
        'verify' => TRUE,
        'http_errors' => FALSE,
        'connect_timeout' => $connectionTimeout,
      ];
      $client = $this->httpClientFactory->fromOptions($clientOptions);

      $headers = [
        'Ocp-Apim-Subscription-Key' => $subscriptionKey,
        'Content-Type' => 'application/json',
        'Authorization' => 'Bearer ' . $token,
      ];

      $response = $client->post($url, [
        'headers' => $headers,
        'body' => Json::encode($data),
      ]);

      if ($response->getStatusCode() == 200) {
        return Json::decode($response->getBody());
      }
      else {
        $this->logger->error(
          $this->t(
            "TuyaPay was unable to make the collection.<br />%reason",
            [
              '%reason' => $response->getReasonPhrase(),
            ],
          )
        );
        return [
          'error' => TRUE,
          'response' => Json::decode($response->getBody()->getContents()),
        ];
      }

    }
    catch (ConnectException $e) {
      watchdog_exception('tuyapay', $e);
      return ['error' => TRUE];
    }

    return ['error' => TRUE];
  }

}
