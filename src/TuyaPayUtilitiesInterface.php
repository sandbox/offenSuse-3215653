<?php

namespace Drupal\commerce_tuyapay;

/**
 * Define interface for TuyaPay commerce.
 */
interface TuyaPayUtilitiesInterface {

  /**
   * Generate Authorization token.
   *
   * @param string $endpoint
   *   Endpoint from gateway configuration for oAuth service.
   * @param string $subscriptionKey
   *   Id provided for TuyaPay set in the gateway configuration.
   * @param array $params
   *   Collection with required data.
   *
   * @return array
   *   Service Response.
   */
  public function generateToken($endpoint, $subscriptionKey, array $params);

  /**
   * Generate OTP Code.
   *
   * @param string $endpoint
   *   Endpoint from gateway configuration for otp service.
   * @param string $subscriptionKey
   *   Id provided for TuyaPay set in the gateway configuration.
   * @param string $token
   *   Autorization token generated for generateToken method.
   * @param array $data
   *   Collection with required data.
   *
   * @return array
   *   Service Response.
   */
  public function generateOtpCode($endpoint, $subscriptionKey, $token, array $data);

  /**
   * Execute recharger.
   *
   * @param string $endpoint
   *   Endpoint from gateway configuration for recharger service.
   * @param string $subscriptionKey
   *   Id provided for TuyaPay set in the gateway configuration.
   * @param string $token
   *   Autorization token generated for generateToken method.
   * @param int $connectionTimeout
   *   Max connection time to complete the request.
   * @param array $data
   *   Collection with required data.
   *
   * @return array
   *   Service Response.
   */
  public function cashOutRecharger($endpoint, $subscriptionKey, $token, $connectionTimeout, array $data);

}
